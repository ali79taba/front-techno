import Container from "react-bootstrap/Container";
import {Col, Row} from "react-bootstrap";
import {Switch} from "antd";
import DatePicker from "react-datepicker2";
import TimePicker from "rc-time-picker";
import {default as qmoment} from "moment";
import React, {useState} from "react";
import moment from "jalali-moment";
import {toast} from "react-toastify";
import api from "../utils/api";
import {mutate} from "swr";

interface TimeSlotFormProps {
    teacherId : number;
}

function TimeSlotForm ({teacherId} : TimeSlotFormProps) {
    const [manual, setManual] = useState(false);
    const [date, setDate] = useState(moment());
    const [endTime, setEndTime] = useState(qmoment().hour(0).minute(0));
    const [dateDescription, setDescription]: any = useState(null);
    const [startTime, setStartTime] = useState(qmoment().hour(0).minute(0));


    async function addTimeSlot() {
        toast("زمان شما اضافه شد");

        api(`/timeslot/${teacherId}`, "POST", {
            startDate: date.hour(startTime.hour()).minute(startTime.minute()).unix(),
            endDate: date.hour(endTime.hour()).minute(endTime.minute()).unix(),
            isDate: manual,
            description: dateDescription,
        }).then(() => {
            mutate(`/timeslot/${teacherId}`);
            setStartTime(qmoment().hour(0).minute(0));
            setEndTime(qmoment().hour(0).minute(0));
            setDescription("");
            api(`/timeslot/${teacherId}`).then((res) => {
                console.log(res)
            });
        })
        // console.log(date, startTime, endTime);
        // console.log(dateDescription);
        // console.log(manual);
    }

    return (
        <Row className="container-main-profile rtl">
            <Container fluid>
                <Row>
                    <p>
                        اضافه کردن تایم اسلات
                    </p>
                </Row>
                <Row>
                    <Col>
                        <label style={{float: "right"}}>
                            <span className={"pl-1"}>{!manual ? "متن" : "تاریخ"}</span>
                            <Switch onClick={() => {
                                setManual(!manual)
                            }}/>
                        </label>
                    </Col>
                    {manual && <>
                        <Col sm={3}>
                            <Row>
                                <span className={"pl-3"}>تاریخ</span>
                                <DatePicker timePicker={false} isGregorian={false}
                                            onChange={(value) => (setDate(value))}/>
                            </Row>
                        </Col>
                        <Col sm={3}>
                            <span className={"pl-2"}>از</span>
                            <TimePicker
                                showSecond={false}
                                value={startTime}
                                className={"time-picker-custom"}
                                onChange={(value) => {
                                    setStartTime(qmoment().hour(value.hour()).minute(value.minute()))
                                }}
                                // value={this.state.time}
                            />
                        </Col>
                        <Col sm={3}>
                            <span className={"pl-2"}>تا</span>
                            <TimePicker
                                value={endTime}
                                showSecond={false}
                                className={"time-picker-custom"}
                                onChange={(value) => {
                                    console.log(value.hour(), value.hours());
                                    setEndTime(qmoment().hours(value.hours()).minute(value.minute()));
                                }}
                                // value={this.state.time}
                            />
                        </Col>
                    </>}
                    {!manual &&
                    <Col>
                        <label className={"pl-3"}>زمان</label>
                        <input type={"text"} value={dateDescription} onChange={(value) => {
                            setDescription(value.target.value)
                        }}/>
                    </Col>
                    }
                    <Col>
                        <button className={"btn btn-success"} onClick={addTimeSlot}>اضافه کردن</button>
                    </Col>
                </Row>
            </Container>
        </Row>
    )
}


export default TimeSlotForm