import useSWR from "swr";
import React from 'react';
import api from "../utils/api";
import {useEffect, useState} from "react";
import {Col, Container, Row} from "react-bootstrap";
import moment from 'jalali-moment';
import Link from "next/link";


interface TimeSlotProps {
    teacherId: number;
}

interface TimeSlotsResponse {
    description?: string;
    id?: number;
    isDate?: boolean;
    teacherId?: number;
    userId?: number;
    startDate?: number;
    endDate?: number;

}

export default function TimeSlots({teacherId}: TimeSlotProps) {
    moment.locale('fa');
    const {data: timeSlots} = useSWR(`/timeslot/${teacherId}`, api);
    const [timeSlotElements, setTimeSlotElements] = useState(<div></div>);
    useEffect(() => {
        console.log("TIME : ", timeSlots, teacherId);
        if (timeSlots && typeof (timeSlots) === 'object' && !('status' in timeSlots)) {
            const arrayOfData = timeSlots.timeSlots;
            setTimeSlotElements(arrayOfData.map((value: TimeSlotsResponse) => {
                return (
                    <Row className={"rtl row-border"}>
                        <Col sm={"2"}>
                            <div className={"text-container"}>
                                <span className={'my-span'}>شماره</span>
                                {value.id}
                            </div>
                        </Col>
                        {value.isDate ?
                            <>
                                <Col md={"2"}>
                                    <div className={"text-container"}>
                                        <span className={'my-span'}>تاریخ</span>
                                        {value.startDate && moment.unix(value.startDate).format("YYYY/MM/DD")}
                                    </div>
                                </Col>
                                <Col md={"1"} className={"rtl"}>
                                    <div className={"text-container"}>
                                        <span className={'my-span'}>از</span>
                                        {value.startDate && moment.unix(value.startDate).format("HH:mm")}
                                    </div>
                                </Col>
                                <Col md={"1"} className={"rtl"}>
                                    <div className={"text-container"}>
                                        <span className={'my-span'}>تا</span>
                                        {value.endDate && moment.unix(value.endDate).format("HH:mm")}
                                    </div>
                                </Col>

                            </> :
                            <Col md={"4"} className={"rtl"}>
                                <div className={"text-container"}>
                                    <span className={'my-span'}>زمان</span>
                                    {value.description}
                                </div>
                            </Col>
                        }
                        <Col md={"2"} className={"rtl"}>
                            <div className={"text-container"}>
                                <span className={'my-span'}>وضعیت</span>
                                {value.userId ? 'ثبت شده' : 'خالی'}
                            </div>
                        </Col>
                        {value.userId &&
                        <Col md={"2"} className={"rtl"}>
                            <Link href={`/student/${value.userId}`}><button className={"btn btn-success"} onClick={()=>{}}>مشاهده دانشجو</button></Link>
                        </Col>
                        }
                    </Row>
                )
            }))
        }
    }, [timeSlots])
    return <Row className="container-main-profile" style={{direction: "rtl"}}>
        <h4 className={"my-h4"}>بازه زمانی های مشاوره</h4>
        <Container fluid>
            {timeSlotElements}
        </Container>
    </Row>
}