import {setEditTeacherMode} from "../store/action-creators/pull-action-creators";
import {connect, useStore} from "react-redux";
import Container from "react-bootstrap/Container";
import {Col, Row} from "react-bootstrap";
import AppState from "../store/app-state-interface";
import React, {useEffect, useState} from "react";
import {store} from "../store/redux-store";
import {getUser} from "../store/tasks/get-user";
import api from "../utils/api";
import {UserState} from "../store/app-state-interface/user-state";
import {mutate} from "swr";
import {NextRouter} from "next/router";

interface teacherComponentProps {
    user?: UserState;
    teacher: any;
    editMode?: boolean;
    editable?: boolean;
}

let formInputText: any = {
    'id': null,
    'code': null,
    'gerayesh': null,
    'field': null,
    'last_name': null,
    'first_name': null,
    'description': null,
    'contact': null,
    'image_link': null,
    'username': null,
    'dontSendRequestNotificationBot': null,

}

const TeacherDetail = (props: teacherComponentProps) => {
    const store = useStore();

    //console.log(store.getState());
    // //console.log(props);
    const teacher = props.teacher;
    const colName: any = {
        'first_name': 'نام',
        'last_name': 'نام خانوادگی',
        'username': 'نام کاربری',
        'code': 'رمز',
        'gerayesh': 'گرایش',
        'field': 'رشته',
        'email': 'ایمیل',
    };

    store.subscribe(() => {
        //console.log("Hey i'm in id teacher : ", store.getState());
    })


    const [columns, setColumns]: any = useState([]);

    useEffect(() => {
        console.log("teacher : ", teacher);
        if (teacher) {
            const columns = [];
            for (const col in colName) {
                let disabledInput: boolean = false;
                if ((col === 'field' || col === 'gerayesh') && store.getState().user?.userType != "admin") {
                    disabledInput = true;
                }

                columns.push(
                    <Col key={col} className={"d-flex m-1"} sm={5}>
                        <div className={"text-container"}>
                            <span className={'my-span'}>{colName[col]} :</span>
                            {!props.editMode ? <span className="profile-detail-span">{teacher[col]}</span> :
                                <input name={col} type={"text"} defaultValue={teacher[col]} className={"form-control"}
                                       ref={(element) => {
                                           formInputText[col] = element
                                       }} disabled={disabledInput}/>}
                        </div>
                    </Col>)
            }
            setColumns(columns);
        }
    }, [teacher, props.editMode, store])


    console.log(props.editMode, props.user, props.editMode && props.user && 'userType' in props.user && (props.user.userType === 'admin' || props.user.userType === 'teacher'));
    return (
        <form>
            <input type={'text'} defaultValue={teacher?.id} ref={(element) => {
                formInputText.id = element
            }} hidden={true}/>
            <Row className="container-main-profile rtl">
                <h4 className={"my-h4 text-right"}>مشخصات شما</h4>
                <p className={"d-flex align-self-center rtl text-right"}>شما با این مشخصات در ربات معرفی می‌شوید. در
                    صورت تمایل به کمک گزینه Edit مشخصات را اصلاح کنید و در پایان Save را بزنید.</p>
                <Row className="w-100">
                    <Col md={7}>
                        <img alt={"profile-image"} className="image-profile" src={teacher?.image_link}/>
                        {props.editMode && props.user && 'userType' in props.user && (props.user.userType === 'admin') &&
                        <input type={'text'} defaultValue={teacher.image_link} className={"form-control"}
                               name={'image_link'} ref={(element) => {
                            formInputText['image_link'] = element
                        }}/>}
                    </Col>
                    <Col md={5}>
                        <Container>
                            <Row className={"rtl teacher-row"}>
                                {columns}
                            </Row>
                            <Row className={"rtl teacher-row"} style={{maxWidth: "inherit"}} >
                                <Col xs="auto">
                                    <div className={"text-container"}>
                                        <span className='my-span'>ارسال اطلاعیه درخواست ها در ربات تلگرام :</span>
                                        {props.editMode && props.user && 'userType' in props.user && (props.user.userType === 'admin' || props.user.userType === 'teacher') ?
                                            <select className={"form-control d-flex"} style={{width: ""}}
                                                    ref={(element) => {
                                                        formInputText['dontSendRequestNotificationBot'] = element
                                                    }}
                                                    defaultValue={(teacher && teacher.dontSendRequestNotificationBot === true) ? "true" : "false"}>
                                                <option value={"false"}>فعال</option>
                                                <option value={"true"}>غیر فعال</option>
                                            </select> :
                                            <span className="profile-detail-span">
                                                {teacher && (teacher.dontSendRequestNotificationBot ? "غیر فعال" : "فعال")}
                                            </span>
                                        }
                                    </div>
                                </Col>
                            </Row>
                            <Row className={"rtl teacher-row"}>
                                <Col>
                                    <div className={"text-container"}>
                                        <span className={'my-span'}>درباره استاد :</span>
                                        {!props.editMode ? <span className="profile-detail-span">{teacher?.description}</span> :
                                            <input name={"description"} defaultValue={teacher.description}
                                                   className={"form-control"} ref={(element) => {
                                                formInputText['description'] = element
                                            }}/>}
                                    </div>
                                </Col>
                            </Row>
                            <Row className={"teacher-row"}>
                                <Col>
                                    <div className="text-container">
                                        <span className={'my-span'}>راه ارتباطی :</span>
                                    </div>
                                </Col>
                                <Col>
                                    {!props.editMode ? <span className="profile-detail-span text-right">{teacher?.contact}</span> :
                                        <textarea name={"contact"} defaultValue={teacher.contact}
                                                  className={"form-control"} rows={4} cols={50}
                                                  ref={(element) => {
                                                      formInputText['contact'] = element
                                                  }}/>}
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                </Row>

            </Row>
        </form>
    )
}

const teacherSubmitHandler = ((router : NextRouter) => {
    // const dispatch = useDispatch();
    // //console.log(formInputText?.focus());
    let body: any = {};
    for (const index in formInputText) {
        body[index] = formInputText[index]?.value;
    }
    console.log(body["dontSendRequestNotificationBot"]);
    body["dontSendRequestNotificationBot"] = body["dontSendRequestNotificationBot"] === "true";
    console.log("DONT SENT : ", body);
    let request : any = null;
    if(router.pathname === '/') {
        request = api('/profile', "PUT", body);
    }else{
        request = api('/teachers/' + body.id, "PUT", body);
    }
    request.then(async () => {
        //console.log("OK");
        getUser();
        await mutate(`/teachers/${body.id}`);
        store.dispatch(setEditTeacherMode(false));
    });
}).bind(TeacherDetail);

export {teacherSubmitHandler};

function mapStateToProps(state: AppState) {
    return {
        editMode: state.pull.mode,
        user: state?.user,
    }
}

export default connect(mapStateToProps)(TeacherDetail);