import Link from "next/link";
import React, {useEffect} from "react";
import {useRouter} from "next/router";
import {connect, useDispatch, useSelector} from "react-redux";
import {setEditTeacherMode} from "../store/action-creators/pull-action-creators";
import AppState from "../store/app-state-interface";
import {teacherSubmitHandler} from "./teacher";
import Cookies from "universal-cookie";
import {cleanUser} from "../store/action-creators/user-action-creators";


function Navbar({editMode}: { editMode: boolean }) {
    const loggedIn = useSelector((state: AppState) => (state.user && ('username' in state.user)));
    const user = useSelector((state: AppState) => state.user);
    const dispatch = useDispatch();

    const cookies = new Cookies();

    useEffect(() => {
        //console.log("LOGGED IN : ",loggedIn);
    })

    //console.log("editMode", editMode);


    function editTeacher() {
        dispatch(setEditTeacherMode(true))
    }

    function editTeacherCancel() {
        dispatch(setEditTeacherMode(false));

    }

    function logoutHandler() {
        cookies.set('token', '', {path: '/', domain: process.env.DOMAIN || "localhost"});
        dispatch(cleanUser());
        //console.log("STORE AFTER CLEAR CATCH : ",store.getState());
        router.push('/login').then();
    }


    const router = useRouter();
    return (<> {loggedIn &&
        <nav className="navbar navbar-light sticky-top rtl navbar-custom" style={{backgroundColor: "#e3f2fd"}}>
            <img src="/android-chrome-512x512.png" className="image-logo"/>
            <div className="nav-main navbar-brand" style={{color: "white"}}>مشاور تکنوتز</div>


            <ul className="navbar-nav ml-auto pr-2 navbar-container">
                <li className="nav-item active hover-nav custom-nav-item">
                    <Link href={'/'}><a className="nav-link">خانه<span className="sr-only">(current)</span></a></Link>
                </li>
                {(loggedIn && user && 'userType' in user && user?.userType === "admin") &&
                <li className="nav-item active hover-nav custom-nav-item">
                    <Link href={"/teachers"}><a className={"hover-nav nav-link"}>اساتید<span
                        className="sr-only">(current)</span></a></Link>
                </li>
                }
                {((router.pathname === '/' && loggedIn && user && 'userType' in user && user?.userType === "teacher") ||
                    (router.pathname.match(/\/teachers\/.+/) && loggedIn && user && 'userType' in user && user?.userType === 'admin')) &&
                (!editMode ?
                    <li className="nav-item active hover-nav custom-nav-item">
                        <a className={"hover-nav nav-link"} onClick={editTeacher}>ویرایش اطلاعات<span
                            className="sr-only">(current)</span></a>
                    </li>
                    :
                    <>
                        <li className="nav-item active hover-nav custom-nav-item">
                            <a className={"hover-nav nav-link"} onClick={editTeacherCancel}>کنسل<span
                                className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item active  hover-nav custom-nav-item">
                            <a className={"hover-nav nav-link"} onClick={()=>{teacherSubmitHandler(router)}}>ذخیره<span
                                className="sr-only">(current)</span></a>
                        </li>
                    </>)
                }
            </ul>
            {loggedIn ?
                <button className="btn btn-outline-success my-2 my-sm-0" onClick={logoutHandler}>خروج</button> :
                <Link href={'/login'}>
                    <button className="btn btn-outline-success my-2 my-sm-0">ورود</button>
                </Link>
            }
        </nav>}
        </>
    )
}

function mapStateToProps(store: AppState) {
    return {
        editMode: store.pull.mode,
    }
}

export default connect(mapStateToProps)(Navbar);