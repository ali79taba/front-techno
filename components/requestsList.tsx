import Link from "next/link";
import React from "react";
import {Row} from "react-bootstrap";

interface IndexProps {
    pending: any;
    accepted: any;
    rejected: any;
}

const RequestList = (props: IndexProps) => {
    const columnListName = ['name', 'field', 'gerayesh', 'grade'];


    const pendingList: any = [];
    for (const index in props.pending) {
        const user = props.pending[index].user;
        const row = columnListName.map(col => {
            const value = user[col];
            //console.log("COOOL :", value);
            return (<td className={"tableData text-center"} key={col}>{value}</td>)
        });
        // row.push(<td className={"tableData text-center"} key={"ok"}><button className="btn btn-outline-primary my-2 my-sm-0" onClick={(e)=>{acceptStudentRequest(e, pending[index].id)}}>تایید</button></td>)
        // row.push(<td className={"tableData text-center"} key={"dismiss"}><button className="btn btn-outline-primary my-2 my-sm-0"  onClick={(e)=>{rejectStudentRequest(e, pending[index].id)}}>رد</button></td>)
        pendingList.push(<Link href={'/student/' + user.id} key={props.pending[index].id}>
            <tr style={{cursor: 'pointer'}} className={'tableRow'}>{row}</tr>
        </Link>)
    }


    const acceptedList: any = [];
    for (const index in props.accepted) {
        const user = props.accepted[index].user;
        const row = columnListName.map(col => {
            const value = user[col];
            //console.log("COOOL :", value);
            return (<td className={"tableData text-center"} key={col}>{value}</td>)
        });
        acceptedList.push(<Link href={'/student/' + user.id} key={props.accepted[index].id}>
            <tr style={{cursor: 'pointer'}} className={'tableRow'}>{row}</tr>
        </Link>)
    }


    const rejectedList: any = [];
    for (const index in props.rejected) {
        const user = props.rejected[index].user;
        const row = columnListName.map(col => {
            const value = user[col];
            //console.log("COOOL :", value);
            return (<td className={"tableData text-center"} key={col}>{value}</td>)
        });
        rejectedList.push(<Link href={'/student/' + user.id} key={props.rejected[index].id}>
            <tr style={{cursor: 'pointer'}} className={'tableRow'}>{row}</tr>
        </Link>)
    }
    return (<>
        <Row className="container-main-profile" style={{direction: "rtl"}}>
            <h4 className={"my-h4"}>درخواست های در انتظار تایید</h4>
            <table className="table">
                <thead>
                <tr>
                    <th scope="col" className={"tableData text-center"}>نام</th>
                    <th scope="col" className={"tableData text-center"}>رشته</th>
                    <th scope="col" className={"tableData text-center"}>گرایش</th>
                    <th scope="col" className={"tableData text-center"}>مقطع تحصیلی</th>
                    {/*<th scope="col" className={"tableData text-center"}>تایید</th>*/}
                    {/*<th scope="col" className={"tableData text-center"}>رد</th>*/}

                </tr>
                </thead>
                <tbody>
                {pendingList}
                </tbody>
            </table>
        </Row>
        <Row className="container-main-profile" style={{direction: "rtl"}}>
            <h4 className={"my-h4"}>درخواست های تایید شده</h4>
            <table className="table">
                <thead>
                <tr>
                    <th scope="col" className={"tableData text-center"}>نام</th>
                    <th scope="col" className={"tableData text-center"}>رشته</th>
                    <th scope="col" className={"tableData text-center"}>گرایش</th>
                    <th scope="col" className={"tableData text-center"}>مقطع تحصیلی</th>
                </tr>
                </thead>
                <tbody>
                {acceptedList}
                </tbody>
            </table>
        </Row>
        <Row className="container-main-profile" style={{direction: "rtl"}}>
            <h4 className={"my-h4"}>درخواست های رد شده</h4>
            <table className="table">
                <thead>
                <tr>
                    <th scope="col" className={"tableData text-center"}>نام</th>
                    <th scope="col" className={"tableData text-center"}>رشته</th>
                    <th scope="col" className={"tableData text-center"}>گرایش</th>
                    <th scope="col" className={"tableData text-center"}>مقطع تحصیلی</th>
                </tr>
                </thead>
                <tbody>
                {rejectedList}
                </tbody>
            </table>
        </Row>
    </>);
}

export default RequestList