import {UserState} from "../app-state-interface/user-state";
import {UserActions} from "../action_types/user-actions";

const setUser = (user: UserState) : UserActions  => {
    return {
        type: "set",
        payload : {
            user
        }
    }
}

const cleanUser = () : UserActions => {
    return {
        type: "set",
        payload : {
            user : null
        }
    }
}

export {setUser, cleanUser}
