export const setEditTeacherMode = (value: boolean) => {
    return {
        type: "set",
        payload : {
            value
        }
    }
}

