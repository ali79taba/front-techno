import {PullActions} from "./pull-actions";
import {UserActions} from "./user-actions";

export type AppAction =
    PullActions |
    UserActions

