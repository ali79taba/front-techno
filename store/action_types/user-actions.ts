import {UserState} from "../app-state-interface/user-state";

export type UserActions =
    {
        type : "set",
        payload :{
            user ?: UserState,
        }
    }


