import api from "../../utils/api";
import {setUser} from "../action-creators/user-action-creators";
import {store} from "../redux-store";
import {UserState} from "../app-state-interface/user-state";

interface GetUser {
    user : UserState
}

export const getUser = function () {
    api('/user').then((res : GetUser)=>{
        //console.log("RESPONSE : ",res);
        store.dispatch(setUser(res.user));
    })
}