// import { useMemo } from 'react'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
// import { persistReducer } from 'redux-persist'
// import storage from 'redux-persist/lib/storage'
import {reducer} from "./reducers";
import AppState from "./app-state-interface";
import {initialStore} from "./initial-store";
// import {Simulate} from "react-dom/test-utils";
// import load = Simulate.load;
// import {Simulate} from "react-dom/test-utils";
// import load = Simulate.load;

/**
 * whitelist in persistConfig means this property in store will save in storage and get after
 */

// const persistConfig = {
//   key: 'booking',
//   storage,
//   whitelist: ['']
// }

export let store : any = null;


export function makeStore(loadedStateFromPageProps : AppState){
  // const persistedReducer = persistReducer(persistConfig, reducer)
  store = createStore(
      reducer,
      loadedStateFromPageProps ? { ...initialStore, ...loadedStateFromPageProps} : initialStore,
      composeWithDevTools(applyMiddleware())
  );
  return store;
}

/**
 * use this method to get create store
 * Warning : only use in _app.tsx and once
 * @param loadedStateFromPageProps
 */

export function initializeStore(loadedStateFromPageProps : AppState) {
  //console.log("CREATE STORE :: ", store, loadedStateFromPageProps);
  if(store) {
    //console.log("STORE IN CREATE :",store.getState());
    return store;
  }
  // return useMemo(() => makeStore(loadedStateFromPageProps), [loadedStateFromPageProps])
  return makeStore(loadedStateFromPageProps);
}

