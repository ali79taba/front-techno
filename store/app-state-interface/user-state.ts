export type UserState = {
    code: string;
    username : string;
    contact : string;
    id: number;
    field : string;
    first_name: string;
    last_name : string;
    gerayesh : string;
    image_link: string;
    description : string;
    userType : "teacher" | "admin" | "student"
    error?: boolean;
} | {

} | null
