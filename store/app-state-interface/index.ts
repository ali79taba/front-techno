import {PullState} from "./pull-state";
import {UserState} from "./user-state";

export default interface AppState {
    pull : PullState;
    user ?: UserState;
}
