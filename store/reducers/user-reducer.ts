import {AppAction} from "../action_types";
import {UserState} from "../app-state-interface/user-state";

export default function UserReducer(state: UserState = null, action : AppAction) : UserState {
    switch (action.type) {
        case 'set':
            if('user' in action.payload){
                if(action.payload.user){
                    return {
                        ...state,
                        ...action.payload.user
                    }
                }else{
                    return null;
                }
            }
            if(state){
                return {
                    ...state
                }
            }else{
                return null;
            }
        default:
            return state;
    }
}