import {combineReducers} from "redux";
import AppState from "../app-state-interface";
import pullReducer from "./pull-reducer";
import UserReducer from "./user-reducer";


export const reducer = combineReducers<AppState, any>({
    pull : pullReducer,
    user : UserReducer
})