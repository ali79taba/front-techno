import {AppAction} from "../action_types";
import {PullState} from "../app-state-interface/pull-state";
import {initialPullState} from "../initial-store/pull-initial-state";


export default function pullReducer(state: PullState = initialPullState, action : AppAction) : PullState {
    switch (action.type) {
        case 'set':
            if('value' in action.payload)
                return {
                    mode : action.payload.value
                }
            return state;
        default:
            return state;
    }
}