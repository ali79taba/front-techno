import Container from "react-bootstrap/Container";
import {Carousel, Col, Row} from "react-bootstrap";

import './index.css'
import {connect} from "react-redux";

import Link from "next/link";
import React, {useEffect, useState} from 'react'
import {GetServerSideProps} from "next";
import {useSelector} from "react-redux";

import AppState from "../store/app-state-interface";
import TeacherDetail from "../components/teacher";
import useSWR,  { mutate } from 'swr'
import api from "../utils/api";
import {useRouter} from "next/router";
import TimeSlots from "../components/timeslots";
import TimeSlotForm from "../components/timeSlotForm";
const columnListName = [ 'name', 'field', 'gerayesh', 'grade'];




const HomePage = () => {
    // const store = useStore();
    const user = useSelector((state:AppState)=>{return state.user});
    const  [content , setContent] = useState(<></>);
    const router = useRouter();
    const [adminContent, setAdminContent] = useState(<></>);
    const [adminMode, setAdminMode] = useState('regular');

    let formInputText : any = {
        'id': null,
        'code' : null,
        'gerayesh' : null,
        'field' : null,
        'last_name' : null,
        'first_name' : null,
        'description': null,
        'contact' : null,
        'image_link': null
    }

    function acceptStudentRequest(e:React.MouseEvent<HTMLButtonElement>, requestId: number){
        e.stopPropagation();
        // //console.log(requestId);
        api(`/accept/${requestId}`, "POST", {ok: "OK"}).then(()=>{mutate('/teachers/' + (user ? ('id' in user && user.id) : null) + '/requests').then()});
    }

    function rejectStudentRequest(e:React.MouseEvent<HTMLButtonElement>, requestId: number){
        e.stopPropagation();
        // //console.log(requestId);
        api(`/reject/${requestId}`, "POST", {ok: "OK"}).then(()=>{mutate('/teachers/' + (user ? ('id' in user && user.id) : null) + '/requests').then()});
    }

    function createTeacherSubmitHandler(e: React.FormEvent<HTMLFormElement>){
        e.preventDefault();
        //console.log("OKOK");
        let body : any = {};
        for(const index in formInputText){
            body[index] = formInputText[index]?.value;
        }
        api('/teachers', "POST", body).then(()=>{
            setAdminMode('regular');
        });
    }

    const {data : bannerList, error: errorGetBanner} = useSWR('/banner', (...args)=>{
        return api(args).then(res=>res);
    }, {
        revalidateOnFocus: false,
        revalidateOnReconnect: false,
        // revalidateOnMount: false,
    })

    const {data : teacherRequests, error: errorLoadingTeacherData} = useSWR('/teachers/' + (user ? ('id' in user && user.id) : null) + '/requests', (...args)=>{
        return api(args).then(res=>res);
    });

    useEffect(()=>{
        console.log("USER :", user);
        if(user && 'error' in user && user.error){
            router.push('/login');
        }
    })

    useEffect(()=>{
        //console.log("data : ", teacherRequests, user)
        if(user && 'userType' in user && user?.userType === "teacher" && teacherRequests && 'pending' in teacherRequests){
            //console.log("OOK I IN DATA");
            const { pending, accepted, rejected} : any = {
                pending: null,
                accepted: null,
                rejected: null,
                ...teacherRequests
            };


            const pendingList : any = [];
            for(const index in pending){
                const user = pending[index].user;
                const row = columnListName.map(col=>{
                    const value = user[col];
                    //console.log("COOOL :", value);
                    return(<td className={"tableData text-center"} key={col}>{value}</td>)
                });
                row.push(<td className={"tableData text-center"} key={"ok"}><button className="btn btn-outline-primary my-2 my-sm-0" onClick={(e)=>{acceptStudentRequest(e, pending[index].id)}}>تایید</button></td>)
                row.push(<td className={"tableData text-center"} key={"dismiss"}><button className="btn btn-outline-primary my-2 my-sm-0"  onClick={(e)=>{rejectStudentRequest(e, pending[index].id)}}>رد</button></td>)
                    pendingList.push(<Link href={'/student/' + user.id} key={pending[index].id}><tr style={{cursor : 'pointer'}} className={'tableRow'}>{row}</tr></Link>)
            }
            const acceptedList : any= [];
            for(const index in accepted){
                const user = accepted[index].user;
                const row = columnListName.map(col=>{
                    const value = user[col];
                    //console.log("COOOL :", value);
                    return(<td className={"tableData text-center"} key={col}>{value}</td>)
                });
                acceptedList.push(<Link href={'/student/' + user.id} key={accepted[index].id}><tr style={{cursor : 'pointer'}} className={'tableRow'}>{row}</tr></Link>)
            }
            const rejectedList : any = [];
            for(const index in rejected){
                const user = rejected[index].user;
                const row = columnListName.map(col=>{
                    const value = user[col];
                    //console.log("COOOL :", value);
                    return(<td className={"tableData text-center"} key={col}>{value}</td>)
                });
                rejectedList.push(<Link href={'/student/' + user.id} key={rejected[index].id}><tr style={{cursor : 'pointer'}} className={'tableRow'}>{row}</tr></Link>)
            }
            setContent(<Container fluid>
                <TeacherDetail teacher={user}/>
                <TimeSlots teacherId={user.id}/>
                <TimeSlotForm teacherId={user.id}/>
                <Row className="container-main-profile" style={{direction : "rtl"}}>
                    <h4 className="my-h4 text-right">درخواست های در انتظار تایید</h4>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col" className={"tableData text-center"}>نام</th>
                            <th scope="col" className={"tableData text-center"}>رشته</th>
                            <th scope="col" className={"tableData text-center"}>گرایش</th>
                            <th scope="col" className={"tableData text-center"}>مقطع تحصیلی</th>
                            <th scope="col" className={"tableData text-center"}>تایید</th>
                            <th scope="col" className={"tableData text-center"}>رد</th>

                        </tr>
                        </thead>
                        <tbody>
                        {pendingList}
                        </tbody>
                    </table>
                </Row>
                <Row className="container-main-profile" style={{direction : "rtl"}}>
                    <h4 className={"my-h4"}>درخواست های تایید شده</h4>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col" className={"tableData text-center"}>نام</th>
                            <th scope="col" className={"tableData text-center"}>رشته</th>
                            <th scope="col" className={"tableData text-center"}>گرایش</th>
                            <th scope="col" className={"tableData text-center"}>مقطع تحصیلی</th>
                        </tr>
                        </thead>
                        <tbody>
                        {acceptedList}
                        </tbody>
                    </table>
                </Row>
                <Row className="container-main-profile rtl justify-content-center">
                    <h4 className="my-h4" style={{height:"fit-content"}}>دیگر اساتید تکنوتز</h4>
                    <Row className="w-100">
                        <Col style={{marginTop:"20px", padding:"0"}}>
                            { !errorGetBanner && bannerList?.imageLinks?.length > 0 && <Carousel className={"mx-auto" + (window.innerWidth > 1000 ? " w-75" : "") }>
                                {bannerList.imageLinks.map((link: string)=>{
                                    return <Carousel.Item >
                                        <img
                                            className="d-block"
                                            src={link}
                                            alt="image"
                                            style={{height: window.innerWidth > 1000 ? "800px" : "275px" , width:"100%"}}
                                        />
                                    </Carousel.Item>
                                })}
                            </Carousel> }
                        </Col>
                    </Row>
                </Row>

            </Container>)
        }


    },[user, teacherRequests, bannerList, errorGetBanner])

    const colName: any = {
        'first_name' : 'نام',
        'last_name' : 'نام خانوادگی',
        'field' : 'رشته',
        'gerayesh' : 'گرایش',
        'username': 'نام کاربری',
        'code' : 'رمز',
        'image_link': 'لینک عکس',
        'email': 'ایمیل'
    };

    let columns : any = [];
    for(const col in colName){
        columns.push(
            <Col key={col} className={"d-flex flex-row-reverse pb-3"} md={3}>
                <div className={"text-container"}>
                    <span className={"my-span"}>{colName[col]}</span>
                    {<input name={col} type={"text"} className={"form-control"} ref={(element) => {formInputText[col] = element}} required={true}/>}
                </div>
            </Col>
        )
    }


    useEffect(()=>{
        if(user && 'userType' in user && user.userType === 'admin'){
            setAdminContent(<><Container fluid><form onSubmit={(e)=>{createTeacherSubmitHandler(e);}}>
                <Row className={"container-main-profile rtl"}>
                    {adminMode === 'regular' &&
                    <Col md={'auto'}>
                        <button type={"button"} className="btn btn-primary my-2 my-sm-0" onClick={()=>{setAdminMode('createTeacher')}}>ایجاد استاد</button>
                    </Col>}

                    {adminMode === 'createTeacher' &&
                    <Col md={'auto'}>
                        <button type={"button"} className="btn btn-primary my-2 my-sm-0" onClick={()=>{setAdminMode('regular')}}>بازگشت</button>
                    </Col>}

                    {adminMode === 'createTeacher' &&
                    <Col md={'auto'}>
                        <button type={"submit"} className="btn btn-primary my-2 my-sm-0" >submit</button>
                    </Col>}

                </Row>

                { adminMode === 'createTeacher' &&
                <>
                    <input type={'text'} ref={(element) => {formInputText.id = element}} hidden={true}/>
                    <Row className="container-main-profile rtl">
                        <Row style={{padding : 30}}>
                            {columns}
                        </Row>
                        <Row className={"justify-content-end"}  style={{padding : 30}}>
                            <div className={"text-container"}>
                                <span className={"my-span"} style={{minWidth: "100px"}}>درباره استاد</span>
                                { <textarea name={"description"}  className={"form-control my-textarea"} ref={(element) => {formInputText['description'] = element}} required={true}/>}
                            </div>
                        </Row>
                        <Row className={"justify-content-end"}  style={{padding : 30}}>
                            <div className={"text-container"}>
                                <span className={"my-span"}>راه ارتباطی</span>
                                {<textarea name={"contact"}  className={"form-control my-textarea"} ref={(element) => {formInputText['contact'] = element}} required={true}/>}
                            </div>
                        </Row>
                    </Row>
                </>}

            </form>
            {/*<RangeDatePicker />*/}

            {/*<Calendar onChange={()=>{console.log(value)}}/>*/}
            </Container></>);
        }
    }, [user, adminMode]);

    return (user && 'userType' in user && user.userType === 'teacher' && (teacherRequests || !errorLoadingTeacherData) ?
        <>{content}</> : user && 'userType' in user && user.userType === 'admin' ?
            <>{adminContent}</> :  <Container style={{marginTop:"300px"}}><Row className="justify-content-md-center"><Col md={'auto'}><img src={'/91.svg'}/></Col></Row></Container>);
}

export const getServerSideProps: GetServerSideProps = async () => {

    return {
        props: {

        },
    }
}

function mapStateToProps(state: AppState){
    return {
        editMode: state.pull.mode
    }
}

export default connect(mapStateToProps)(HomePage);


