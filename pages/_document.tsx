import Document, {Html, Head, Main, NextScript} from "next/document";
import React from 'react'


export default class CustomDocument extends Document {
    // static async getInitialProps(ctx) {
    // ...
    // }

    render() {
        return (
            <Html lang="en">
                <Head title={"technothes"}>
                    <title>تکنوتز | پنل استاد</title>
                    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/>
                    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
                    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
                    <link rel="manifest" href="/site.webmanifest"/>
                    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5"/>
                    <meta name="msapplication-TileColor" content="#da532c"/>
                    <meta name="theme-color" content="#ffffff"/>
                    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                </Head>
                <body>
                <Main/>
                <NextScript/>
                </body>
            </Html>
        );
    }
}