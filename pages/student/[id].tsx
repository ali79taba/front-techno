import React from 'react'
// import type {GetServerSideProps } from "next";
import Container from "react-bootstrap/Container";
import {Col, Row} from "react-bootstrap";

// import './index.css'
import { useStore} from "react-redux";
// import AppState from "../../store/app-state-interface";
// import Link from "next/link";
// import TeacherDetail from "../../components/teacher";
import {useRouter} from "next/router";
import useSWR from 'swr';
import api from "../../utils/api";

// interface IndexProps {
//     // use : any;
//     // editMode : boolean;
//     // pending: any;
//     // accepted: any;
//     // rejected: any;
// }

// let formInputText : any = {
//     'id': null,
//     'code' : null,
//     'gerayesh' : null,
//     'field' : null,
//     'last_name' : null,
//     'first_name' : null,
//     'description': null,
//     'contact' : null,
//     'image_link': null
// }





const Index = () => {
    const store = useStore();
    const userId = useRouter().query.id;
    const {data : user} = useSWR(`/student/${userId}`, (...args)=>{ return api(args).then(res=>res)});




    //console.log(store.getState());
    const colName: any = {
        'grade': 'مقطع',
        'phone_number': 'شماره همراه',
        'uni': 'دانشگاه',
        'gerayesh' : 'گرایش',
        'field' : 'رشته',
        'name' : 'نام'
    };

    store.subscribe(()=>{
        //console.log("Hey i'm in id teacher : ", store.getState());
    })


    let columns = [];
    if(user){
        for(const col in colName){
            if(colName.hasOwnProperty(col) && col in user)
                columns.push(
                    <Col key={col} className={"d-flex flex-row-reverse"}>
                        <div className={"text-container"}>
                            <span className={"my-span"}>{colName[col]}</span>
                            {user[col]}
                        </div>
                    </Col>
                )
        }
    }


    return(<Container className="container-main-profile-non-fluid">
        <Row className={'p-3'}>{columns}</Row>
        {user ?
            <Row className={'p-3'}>
                <Col key={'intresting'} className={"d-flex flex-row-reverse"}>
                    <div className={"text-container"}>
                        <span className={"my-span"}>علاقه مندی ها</span>
                        {user.intresting}
                    </div>
                </Col>
            </Row> :
            null
        }
    </Container>)
}


// export const getServerSideProps: GetServerSideProps = async ({query}) => {
//     const res = await fetch('http://localhost:3000/teachers/' + query.id);
//     let teacherReceivingRequests = await fetch('http://localhost:3000/teachers/' + query.id + '/requests')
//     const teacher = await res.json();
//     teacherReceivingRequests = await teacherReceivingRequests.json();
//     return {
//         props: {
//             teacher,
//             ...teacherReceivingRequests
//         }
//     }
// }

// function mapStateToProps(state: AppState){
//     return {
//         editMode: state.pull.mode
//     }
// }
//
// export default connect(mapStateToProps)(teacher);

export default Index;