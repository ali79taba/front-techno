import Cookies from "universal-cookie";
import React, {FormEvent, useState} from "react";
import api from "../../utils/api";
import {Container} from "react-bootstrap";
import {useDispatch} from "react-redux";
import {setUser} from "../../store/action-creators/user-action-creators";
import {useRouter} from "next/router";
// import useSWR from 'swr';

export default function LoginPage(){
    const router = useRouter();
    const dispatch = useDispatch();

    const cookies = new Cookies();
    const [username, setUsername] : any = useState(null);
    const [password, setPassword] : any = useState(null);

    async function submitHandler(form: FormEvent){
        form.preventDefault();
        const res = await api('/login', "POST", {
                username : username,
                password : password
            });
        //console.log("REEEEEEES",res);
        // //console.log(cookies.set('karim',''));
        // //console.log(res);
        //console.log(username, " ", password);

        //console.log("RES :",res );
        if(res && 'token' in res){
            console.log("OOOKKK", res);
            console.log("DOMAIN COOKEI", process.env.DOMAIN);
            cookies.set('token', res.token, {path: '/', domain: process.env.DOMAIN || "http://localhost/", sameSite: "none"});
            if('teacher' in res)
                dispatch(setUser({...res.teacher, error:false}));
            if('admin' in res)
                dispatch(setUser({...res.admin, error:false}));
            router.push('/').then();
        }
    }
    // const router = useRouter();
    // useEffect(()=>{
    //     router.push('/teachers');
    // })
    // router.push('/teachers');

    return(
        <Container className="loginPage">
            <form>
                <h3>Sign In</h3>

                <div className="form-group">
                    <label>Username</label>
                    <input type="text" className="form-control" placeholder="Enter email" required onChange={(e)=>{ setUsername(e.target.value) }}/>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" required onChange={(e)=>{ setPassword(e.target.value) }}/>
                </div>

                {/*<div className="form-group">*/}
                {/*    <div className="custom-control custom-checkbox">*/}
                {/*        <input type="checkbox" className="custom-control-input" id="customCheck1" />*/}
                {/*        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>*/}
                {/*    </div>*/}
                {/*</div>*/}

                <button type="submit" data-sitekey="6LfB2-QZAAAAAEA5IvoRRWoMeBgT7Ca_Z-dpHiif"  className="btn btn-primary btn-block g-recaptcha" onClick={submitHandler}>ورود</button>
                {/*<p className="forgot-password text-right">*/}
                {/*    Forgot <a href="#">password?</a>*/}
                {/*</p>*/}
                <div className={"mt-4 mx-auto login-info rtl"}>
                    <p className="text-right">اگر در ورود به سایت مشکل دارید،لطفا به ایدی ادمین در تلگرام پیام دهید</p>
                    <p className={"text-center"}>آیدی ادمین : <a href={"https://t.me/Technothes_Admin"}>@Technothes_Admin</a></p>
                </div>
            </form>
        </Container>

    )
}