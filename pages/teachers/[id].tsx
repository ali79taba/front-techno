import React from 'react'
import type {GetServerSideProps} from "next";
import Container from "react-bootstrap/Container";

import './index.css'
import {connect} from "react-redux";
import AppState from "../../store/app-state-interface";
import TeacherDetail from "../../components/teacher";
import RequestList from "../../components/requestsList";

import useSWR from 'swr';
import api from "../../utils/api";
import TimeSlots from "../../components/timeslots";
import TimeSlotForm from "../../components/timeSlotForm";


interface IndexProps {
    teacherId : number
}


const Teacher = (props: IndexProps) => {

    const {data: teacher} = useSWR(`/teachers/${props.teacherId}`, api);
    const {data: teacherRequest} = useSWR(`/teachers/${props.teacherId}/requests`, api);


    return (<Container fluid>
        <TeacherDetail teacher={teacher}/>
        <TimeSlots teacherId={props.teacherId}/>
        <TimeSlotForm teacherId={props.teacherId}/>
        <RequestList pending={teacherRequest?.pending} accepted={teacherRequest?.accepted} rejected={teacherRequest?.rejected}/>
    </Container>)
}


export const getServerSideProps: GetServerSideProps = async ({params}) => {
    return {
        props: {
            teacherId: params?.id
        }
    }
}

function mapStateToProps(state: AppState) {
    return {
        editMode: state.pull.mode
    }
}

export default connect(mapStateToProps)(Teacher);