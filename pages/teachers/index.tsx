import React from 'react'
import type {GetServerSideProps} from "next";
import useSWR from 'swr';

// interface IndexProps {
//     teachers : {
//         [index:number]: any
//     }
// }

const colName = [ 'first_name' , 'last_name', 'field', 'gerayesh', 'code'].reverse();

import Container from 'react-bootstrap/Container'
import {Col, Row} from "react-bootstrap";
import '../index.css'
import Link from "next/link";
import api from "../../utils/api";
// import api from "../../utils/api";

const Home = () => {
    let items : Array<any> = []
    const {data} = useSWR('/teachers', api)

    //console.log("teachers", data);
    for(const id in data){
        const teacher = data[id];
        let columns = [];
        // columns.push(<Col key={'image_link'+teacher.id} className="col-app">
        //     <img src={teacher['image_link']} className="profile-image"/>
        // </Col>)
        for(const col in colName){
            columns.push(<Col key={col+teacher.id} className="col-app">
                {teacher[colName[col]]}
            </Col>)
        }
        console.log("teacherID :", teacher.id);
        items.push(
            <Link href={"/teachers/" + teacher.id} key={"COLUMNS"+ teacher.id}>
                <div className="row-teacher" key={teacher.id}>
                    <Row className="align-items-center" key={"ROW" + teacher.id}>{columns}</Row>
                </div>
            </Link>
        )

    }
    // //console.log(teachers);

    return(
        <>
            <Container fluid className="container-main">
                <div className="row-teacher">
                    <Row className="align-items-center" key={"ROW"}>
                        <Col className={"col-app"}>رمز</Col>
                        <Col className={"col-app"}>گرایش</Col>
                        <Col className={"col-app"}>رشته</Col>
                        <Col className={"col-app"}>نام خانوادگی</Col>
                        <Col className={"col-app"}>نام</Col>

                    </Row>
                </div>
                { items }
            </Container>
        </>
    )
}

export const getServerSideProps: GetServerSideProps = async () => {
    //console.log(context.req.headers);
    // const res = await api('/teachers');
    // if(!res.ok){
    return {
        props: {

        }
    }
    // }
    // const teachers = await res.json();
    // return {
    //     props: {
    //         teachers
    //     }
    // }
}

export default Home;