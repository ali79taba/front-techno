import React, {useEffect} from 'react';
import 'antd/dist/antd.css'
import 'react-toastify/dist/ReactToastify.css';
import {Provider} from 'react-redux';
// import { PersistGate } from 'redux-persist/integration/react';
import {initializeStore} from '../store/redux-store';
// import { persistStore } from 'redux-persist'
import type { AppProps } from 'next/app'
import 'bootstrap/dist/css/bootstrap.min.css';
// import {useRouter} from "next/router";
import './index.css'
import Navbar from "../components/navbar";
import useSWR from "swr";
import api from "../utils/api";
// import Cookies from "universal-cookie/lib";
import {setUser} from "../store/action-creators/user-action-creators";
import {ToastContainer} from "react-toastify";
// import 'react-modern-calendar-datepicker/lib/DatePicker.css';
// import Cookies from "universal-cookie";
import "./global.css"



const App = function App( { Component , pageProps = { initialReduxState : null} } : AppProps) {
    const {data, error: isGetUserDataError} = useSWR('/user', () => {
        return api('/user').then(res=>res);
    })
    const store = initializeStore({
        ...pageProps.initialReduxState,
        user : data?.user,
    });

    useEffect(()=>{
        if(isGetUserDataError){
            store.dispatch(setUser({error : true}));
        }else if(data && 'user' in data){
            store.dispatch(setUser({
                ...data.user,
                error: false,
            }));
        }
        // console.log("EERORORR" , isGetUserDataError)
        //console.log("STTTTTTTTTOR",store.getState());
    },[data, isGetUserDataError])

    // const cookies = new Cookies();
    // const router = useRouter()


    //console.log(router.pathname);
    return(
        <Provider store={store}>
            {/*<PersistGate persistor={persistStore(store)} loading={<Component {...pageProps} />}>*/}
                <Navbar/>
                <ToastContainer />
                <Component {...pageProps} />
            {/*</PersistGate>*/}
        </Provider>
    )
}




export default App
 
