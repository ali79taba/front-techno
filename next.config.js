const withCSS = require('@zeit/next-css')

module.exports = withCSS({
    cssLoaderOptions: {
        url: false
    },
    env:{
        HOST:"https://thecnothes.liara.run",
	DOMAIN:"https://thecnothes.liara.run"
    },
    experimental: {
        css: true
    }
});
