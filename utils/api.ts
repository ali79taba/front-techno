// import Cookies from "universal-cookie";
export default async function api(url: string, method : "GET" | "POST" | "PUT" = "GET", body ?: any){
    // const cookies = new Cookies();
    //console.log("COOOOOKIE :", cookies.get('token'));
    const host = process.env.HOST || "http://localhost:3000";
    console.log("HOST :", host);
    if(method === "GET"){
        console.log("SEND");
        const res = await fetch(host + url, {
            method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Cache': 'no-cache'
            },
            credentials: 'include',
        })
        //console.log(res);
        if(res.status !== 200)
            throw res;
        return await res.json();
    }else{
        console.log("SEND");
        const res = await fetch(host + url, {
            method,
            body: JSON.stringify(body),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Cache': 'no-cache'
            },
            credentials: 'include',
        })
        if(res.status === 200)
            return await res.json();
        return res;
    }

}